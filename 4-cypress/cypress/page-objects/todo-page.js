export class TodoPage {
    navigate(){
        cy.visit('https://todomvc.com/examples/vanillajs/')
    }

    addTodo(text){
        cy.get('input.new-todo').type(text + '{enter}')
    }

    validateTodoText(index, expectedText){
        cy.get(`.todo-list li:nth-child(${index + 1}) > div > label`).should('have.text', expectedText)
    }
}
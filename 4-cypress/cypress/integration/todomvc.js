const { TodoPage } = require("../page-objects/todo-page");

describe('My First Test', () => {

    const todoPage = new TodoPage;

    beforeEach(()=>{
        todoPage.navigate();
        todoPage.addTodo('write code')
    })

    it('Should add todo', () => {
        todoPage.validateTodoText(0, 'write code')

        cy.get('ul > li > div > input').should('not.be.checked')
    });

    it('Should toggle to completed', () => {
        cy.get('.toggle').click()
        cy.get('ul > li > div > input').should('be.checked')
    });

    it('Should clear completed todos', () => {
        cy.get('.toggle').click()
        cy.contains('Clear').click()
        cy.get('.todo-list').should('not.have.descendants', 'li')
    });

        /* cy.get('.new-todo', {timeout: 2000}).type('do reviews{enter}')
        cy.get('.new-todo', {timeout: 2000}).type('make lunch{enter}')
        cy.get('.new-todo').type('eat lunch{enter}')
        cy.get('.new-todo').type('coffe time, again {enter}') */


    /*     
    it('Add elements on todo list', () => {
            cy.get('.new-todo').type('write code{enter}')
            cy.get('.new-todo').type('do reviews{enter}')
            cy.get('.new-todo').type('make lunch{enter}')
            cy.get('.new-todo').type('eat lunch{enter}')
            cy.get('.new-todo').type('coffe time, again {enter}')
        });
    
        it('Verify number of elements on list', () => {
            cy.get('.todo-list li')
                .should(($li) => {
                    expect($li).to.have.length(5)
                    return
                })
        });
    
        it('Mark tasks as completed', () => {
            cy.get('.todo-list li:first input').should('have.class', 'toggle').click()
            cy.get('.todo-list li:last input').should('have.class', 'toggle').click()
        });
     */
});
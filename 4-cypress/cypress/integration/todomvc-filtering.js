describe('Filtering todos', () => {
    beforeEach(()=> {
        cy.visit('https://todomvc.com/examples/vanillajs/')

        cy.get('input.new-todo').type('write code{enter}')
        cy.get('.new-todo', {timeout: 2000}).type('do reviews{enter}')
        cy.get('.new-todo').type('make lunch{enter}')
        cy.get('.new-todo').type('eat lunch{enter}')
        cy.get('.new-todo').type('coffe time, again {enter}')

        cy.get("ul > li:nth-child(2) > div > input").click()
    });

    it('Should filter active todos', () => {
        cy.contains('Active').click()

        cy.get('.todo-list li').should('have.length', 4)
    });

});
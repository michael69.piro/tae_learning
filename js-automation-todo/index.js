/* 

Objetivo: crear un bot que interactue con la app, como si fuera un usuario.

*/

const autoTodo = new function() {
    this.clickItem = function(item){
        item.click();
    };

    this.toggleAll = function(){
        document.querySelector('#toggle-all').click();
    };

    this.selectItemX = function(x){
        document.querySelector("ul.todo-list > li:nth-child(" + x + ") input.toggle").click();
    };

    this.deleteItemX = function(x){
        document.querySelector("ul.todo-list > li:nth-child(" + x + ") button.destroy").click();
    };

    this.clearCompleted = function(){
        document.querySelector('button.clear-completed').click();
    };
    
    this.filterCompleted = function(){
        location.hash = '/completed';
    };

    this.filterAll = function(){
        location.hash = '/';
    };

    this.filterActive = function(){
        location.hash = '/active'
    };

    this.createTodo = function(name){
        document.querySelector('input.new-todo').value = name;
        document.querySelector('input.new-todo').dispatchEvent(new Event('change', { 'bubbles': true }));
    }

    this.amendTodo = function(x, amendedValue){
        document.querySelector('ul.todo-list > li:nth-child(' + x + ') > div > label').dispatchEvent(new Event('dblclick', { 'bubbles': true }));
        document.querySelector('ul.todo-list > li:nth-child(' + x + ') .edit').value = amendedValue;
    }
}

for (var prop in autoTodo){
    if(typeof autoTodo[prop] == 'function'){
        console.log(prop);
    }
}

var rando = new function(){

    function getRandomInt(x){
        return Math.floor(Math.random()* x );
    }

    function getRandomItemIndex(){
        max = document.querySelectorAll('ul.todo-list li').length;
        if(max === 0){
            console.log('no items to choose from');
            return 0;
        }
        x = getRandomInt(max) + 1;
        return x;
    }

    this.toggleAll = function(){
        console.log('toggle all');
        autoTodo.toggleAll();
    };

    this.selectRAndomItem = function(){
        x = getRandomItemIndex();
        if(x > 0){
            console.log('select item' + x);
            autoTodo.selectItemX(x);
        }
    };

    this.createRandomTodo = function(){
        console.log('create a todo');
        autoTodo.createTodo('random todo' + Date.now());
    };

    this.amendRandomTodo = function(){
        x = getRandomItemIndex();
        if (x > 0) {
            console.log('amend todo');
            autoTodo.amendTodo(x, 'amended random todo' + Date.now());
        }
    };
}


var randoBot = setInterval(function(){
    var theFunctions = [];

    for(var prop in rando){
        if(typeof rando[prop] == 'function'){
            theFunctions.push(prop);
        }
    };

    var randomFunctionIndex = Math.floor(Math.random() * theFunctions.length);

    rando[theFunctions[randomFunctionIndex]]();
}, 1000)

clearInterval(randoBot);
/* 

    this.clearCompleted = function(){
        console.log('clear completed');
        autoTodo.clearCompleted();
    };

    this.filterAll = function(){
        console.log('filter completed');
        autoTodo.filterCompleted();
    };

    this.filterActive = function(){
        console.log('filter active');
        autoTodo.filterActive();
    }

    this.createRandomTodo = function(){
        console.log('create todo');
        autoTodo.createRandomTodo('random todo' + Date.now());
    }


*/
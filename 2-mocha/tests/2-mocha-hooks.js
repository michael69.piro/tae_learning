describe('Mocha Hooks', () => {

    before('Run before all test.', () => {
        console.log('Run before all test.');
    });

    beforeEach('Run before each test.', ()=>{
        console.log('Run before each test.');
    });

    after('Run after all test.', ()=>{
        console.log('Run after all test.');

    });

    afterEach('Run after each test.', ()=>{
        console.log('Run after each test.');

    });

    it('Test Hooks execution.', () => {
        console.log('Test Hooks execution.')
    });
});
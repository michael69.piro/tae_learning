// Test features

// 1. exclusive - .only() - we can .noly to each test in order to run just those.
// 2. inclusive - .skip() - pending status.
// 3. pending - when we dosn't pass a callback fn.

var assert = require('assert');

describe('Mathematical Operations - Test Features', function(){ 
    /* beforeEach('Run before each test.', function(done){
        this.timeout(500);
        setTimeout(done,3000);
    }); */

    var a = 10;
    var b = 10;

    // this.timeout(5000);
    //it.only('Addition of two numbers', () => {
    it('Addition of two numbers', (done) => {
        
        // Test timeout level
        // this.timeout(500);
        setTimeout(done,200);

        var c = a + b;

        assert.equal(c, 20)
    });
    it.skip('Substraction of two numbers', () => {
        var c = a - b;
        assert.equal(c, 0)
    });
    it('Multiplication of two numbers', () => {
        var c = a * b;
        assert.equal(c, 100)
    });
    it('Division of two numbers', () => {
        var c = a / b;
        assert.equal(c, 1)
    });
    it('This is a Pending test')
 });
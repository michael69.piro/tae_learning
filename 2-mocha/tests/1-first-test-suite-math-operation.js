// Test suit - Mathematical Operations 

// Test cases

// 1. Addition
// 2. Substraction
// 3. Multiplication
// 4. Division

var assert = require('assert');

describe('Mathematical Operations - Test Suite', () => { 
    it('Addition of two numbers', () => {
        var a = 10;
        var b = 10;

        var c = a + b;

        assert.equal(c, 20)
    });
    it('Substraction of two numbers', () => {
        var a = 10;
        var b = 10;

        var c = a - b;
        assert.equal(c, 0)
    });
    it('Multiplication of two numbers', () => {
        var a = 10;
        var b = 10;

        var c = a * b;
        assert.equal(c, 100)
    });
    it('Division of two numbers', () => {
        var a = 10;
        var b = 10;

        var c = a / b;
        assert.equal(c, 1)
    });

 });
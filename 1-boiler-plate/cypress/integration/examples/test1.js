describe('My First Test', () => {
  it('Gets, types and asserts', () => {
    cy.visit('https://todomvc.com/examples/vanillajs/')

    // Add elements on the todo list. 
    /* To do, make an object, write fn to loop the object */
    cy.get('.new-todo').type('write code{enter}')
    cy.get('.new-todo').type('do reviews{enter}')
    cy.get('.new-todo').type('make lunch{enter}')
    cy.get('.new-todo').type('eat lunch{enter}')
    cy.get('.new-todo').type('coffe time, again {enter}')

    // Verify number of elements on list 
    cy.get('.todo-list li')
      .should(($li) => {
        expect($li).to.have.length(5)
        return
      })
      
    // Mark task as completed, first element and last element.
    cy.get('.todo-list li:first input').should('have.class', 'toggle').click()
    cy.get('.todo-list li:last input').should('have.class', 'toggle').click()

  })
})
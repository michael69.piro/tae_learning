/* 

** expect API examples.
** // URL: 
        https://www.chaijs.com/guide/styles/#expect
        https://www.chaijs.com/guide/styles/#should
        https://www.chaijs.com/guide/styles/#assert
*/

const chai = require('chai');
// expect, sholud

const expect = chai.expect;
const should = chai.should();
const assert = chai.assert;

let a = 1, b = 1;

expect(a).to.be.equals(b, 'a and b are not equal');
// should
a.should.be.equals(b);
// assert
assert.equal(a, b, 'a an b are not equal');

// object/string/boolean
function myObj(){
    return{
        a: 100,
        b: "Hello"
    }
}

x = new myObj(), y = new myObj();
expect(x).to.be.an("object");
// expect(x).to.be.equals(y, "x and y are not equal");

// deep.equals 
// assertion error because expect dont compare value of the object.
expect(x).to.be.deep.equals(y, "x and y are not equal");
x.should.be.deep.equals(y, "x and y are not equal");
assert.deepEqual(x, y, 'x and y are not equals')

// chaining expressions
expect(x).to.be.an('object').and.to.be.deep.equals(y, "x and y are not equal");
(x).should.be.an('object').and.to.be.deep.equals(y)

// arrays 
let numbers = [1, 2, 3, 0];

expect(numbers).to.be.an('array').that.includes(0);
(numbers).should.be.an('array').that.includes(0);
assert.isArray(numbers, 'numbers is not an array')
const chai = require('chai');
const should = chai.should();

/* 

    Should extras
    should -> Object.Prototype - a.should
    existence of an object

    should.exist
    should.not.exist
    should.equal
    should.not.equal
    should.Throw
    should.not.Throw

*/
function writeToAFile(error){
    // normal scenario 
    // error.should.not.exist();

    // Given that error is undefined
    should.not.exist(error);
}

// var error = 1
var error = undefined
writeToAFile(error);